## Things Working

``` bash
(tools) [neteng@devel ansible-play-books]$ ansible-playbook -i netbox_inv.yaml get_2.yaml  -vvvvvv -k
ansible-playbook [core 2.11.6]
  config file = /home/neteng/ansible-play-books/ansible.cfg
  configured module search path = ['/home/neteng/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/neteng/tools/lib64/python3.8/site-packages/ansible
  ansible collection location = /home/neteng/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/neteng/tools/bin/ansible-playbook
  python version = 3.8.6 (default, Jan 29 2021, 17:38:16) [GCC 8.4.1 20200928 (Red Hat 8.4.1-1)]
  jinja version = 3.0.2
  libyaml = True
Using /home/neteng/ansible-play-books/ansible.cfg as config file
SSH password:
setting up inventory plugins
host_list declined parsing /home/neteng/ansible-play-books/netbox_inv.yaml as it did not pass its verify_file() method
script declined parsing /home/neteng/ansible-play-books/netbox_inv.yaml as it did not pass its verify_file() method
Loading collection netbox.netbox from /home/neteng/.ansible/collections/ansible_collections/netbox/netbox
Fetching: http://192.168.2.183:8000/api/docs/?format=openapi
Fetching: http://192.168.2.183:8000/api/dcim/devices/?limit=0&exclude=config_context
Fetching: http://192.168.2.183:8000/api/virtualization/virtual-machines/?limit=0&exclude=config_context
Fetching: http://192.168.2.183:8000/api/dcim/sites/?limit=0
Fetching: http://192.168.2.183:8000/api/dcim/regions/?limit=0
Fetching: http://192.168.2.183:8000/api/dcim/locations/?limit=0
Fetching: http://192.168.2.183:8000/api/tenancy/tenants/?limit=0
Fetching: http://192.168.2.183:8000/api/dcim/racks/?limit=0
Fetching: http://192.168.2.183:8000/api/dcim/device-roles/?limit=0
Fetching: http://192.168.2.183:8000/api/dcim/platforms/?limit=0
Fetching: http://192.168.2.183:8000/api/dcim/device-types/?limit=0
Fetching: http://192.168.2.183:8000/api/dcim/manufacturers/?limit=0
Fetching: http://192.168.2.183:8000/api/virtualization/clusters/?limit=0
Fetching: http://192.168.2.183:8000/api/ipam/services/?limit=0
Parsed /home/neteng/ansible-play-books/netbox_inv.yaml inventory source with auto plugin
Loading collection juniper.device from /home/neteng/.ansible/collections/ansible_collections/juniper/device
Loading callback plugin default of type stdout, v2.0 from /home/neteng/tools/lib64/python3.8/site-packages/ansible/plugins/callback/default.py
Attempting to use 'default' callback.
Skipping callback 'default', as we already have a stdout callback.
Attempting to use 'junit' callback.
Attempting to use 'minimal' callback.
Skipping callback 'minimal', as we already have a stdout callback.
Attempting to use 'oneline' callback.
Skipping callback 'oneline', as we already have a stdout callback.
Attempting to use 'tree' callback.

PLAYBOOK: get_2.yaml *******************************************************************************************************************
Positional arguments: get_2.yaml
verbosity: 6
ask_pass: True
connection: smart
timeout: 10
become_method: sudo
tags: ('all',)
inventory: ('/home/neteng/ansible-play-books/netbox_inv.yaml',)
forks: 4
1 plays in get_2.yaml

PLAY [Get Device Facts] ****************************************************************************************************************
META: ran handlers

TASK [Retrieve facts from devices running Junos OS] ************************************************************************************
task path: /home/neteng/ansible-play-books/get_2.yaml:8
<192.168.2.50> ESTABLISH LOCAL CONNECTION FOR USER: neteng
<192.168.2.50> EXEC /bin/sh -c 'echo ~neteng && sleep 0'
<192.168.2.50> EXEC /bin/sh -c '( umask 77 && mkdir -p "` echo /home/neteng/.ansible/tmp `"&& mkdir "` echo /home/neteng/.ansible/tmp/ansible-tmp-1635698549.2555184-172465-37191165215146 `" && echo ansible-tmp-1635698549.2555184-172465-37191165215146="` echo /home/neteng/.ansible/tmp/ansible-tmp-1635698549.2555184-172465-37191165215146 `" ) && sleep 0'
Including module_utils file ansible/__init__.py
Including module_utils file ansible/module_utils/__init__.py
Including module_utils file ansible/module_utils/_text.py
Including module_utils file ansible/module_utils/basic.py
Including module_utils file ansible/module_utils/common/_collections_compat.py
Including module_utils file ansible/module_utils/common/__init__.py
Including module_utils file ansible/module_utils/common/_json_compat.py
Including module_utils file ansible/module_utils/common/_utils.py
Including module_utils file ansible/module_utils/common/arg_spec.py
Including module_utils file ansible/module_utils/common/file.py
Including module_utils file ansible/module_utils/common/parameters.py
Including module_utils file ansible/module_utils/common/collections.py
Including module_utils file ansible/module_utils/common/process.py
Including module_utils file ansible/module_utils/common/sys_info.py
Including module_utils file ansible/module_utils/common/text/converters.py
Including module_utils file ansible/module_utils/common/text/__init__.py
Including module_utils file ansible/module_utils/common/text/formatters.py
Including module_utils file ansible/module_utils/common/validation.py
Including module_utils file ansible/module_utils/common/warnings.py
Including module_utils file ansible/module_utils/compat/selectors.py
Including module_utils file ansible/module_utils/compat/__init__.py
Including module_utils file ansible/module_utils/compat/_selectors2.py
Including module_utils file ansible/module_utils/compat/selinux.py
Including module_utils file ansible/module_utils/distro/__init__.py
Including module_utils file ansible/module_utils/distro/_distro.py
Including module_utils file ansible/module_utils/errors.py
Including module_utils file ansible/module_utils/parsing/convert_bool.py
Including module_utils file ansible/module_utils/parsing/__init__.py
Including module_utils file ansible/module_utils/pycompat24.py
Including module_utils file ansible/module_utils/six/__init__.py
Including module_utils file ansible_collections/juniper/device/plugins/module_utils/configuration.py
Including module_utils file ansible_collections/__init__.py
Including module_utils file ansible_collections/juniper/__init__.py
Including module_utils file ansible_collections/juniper/device/__init__.py
Including module_utils file ansible_collections/juniper/device/plugins/__init__.py
Including module_utils file ansible_collections/juniper/device/plugins/module_utils/__init__.py
Including module_utils file ansible_collections/juniper/device/plugins/module_utils/juniper_junos_common.py
Including module_utils file ansible/module_utils/connection.py
Including module_utils file ansible/module_utils/common/json.py
Using module file /home/neteng/.ansible/collections/ansible_collections/juniper/device/plugins/modules/facts.py
<192.168.2.50> PUT /home/neteng/.ansible/tmp/ansible-local-172393uw6fe4x6/tmphkjtgojq TO /home/neteng/.ansible/tmp/ansible-tmp-1635698549.2555184-172465-37191165215146/AnsiballZ_facts.py
<192.168.2.50> EXEC /bin/sh -c 'chmod u+x /home/neteng/.ansible/tmp/ansible-tmp-1635698549.2555184-172465-37191165215146/ /home/neteng/.ansible/tmp/ansible-tmp-1635698549.2555184-172465-37191165215146/AnsiballZ_facts.py && sleep 0'
<192.168.2.50> EXEC /bin/sh -c '~/tools/bin/python /home/neteng/.ansible/tmp/ansible-tmp-1635698549.2555184-172465-37191165215146/AnsiballZ_facts.py && sleep 0'
<192.168.2.50> EXEC /bin/sh -c 'rm -f -r /home/neteng/.ansible/tmp/ansible-tmp-1635698549.2555184-172465-37191165215146/ > /dev/null 2>&1 && sleep 0'
ok: [vmx01] => {
    "ansible_facts": {
        "junos": {
            "HOME": "/var/home/neteng",
            "RE0": {
                "last_reboot_reason": "0x10:misc hardware reason",
                "mastership_state": "master",
                "model": "RE-VMX",
                "status": "OK",
                "up_time": "11 minutes, 45 seconds"
            },
            "RE1": null,
            "RE_hw_mi": false,
            "current_re": [
                "master",
                "node",
                "fwdd",
                "member",
                "pfem",
                "re0"
            ],
            "domain": "kpn",
            "fqdn": "ASH1-INT-03.kpn",
            "has_2RE": false,
            "hostname": "ASH1-INT-03",
            "hostname_info": {
                "re0": "ASH1-INT-03"
            },
            "ifd_style": "CLASSIC",
            "junos_info": {
                "re0": {
                    "object": {
                        "build": 8,
                        "major": [
                            14,
                            1
                        ],
                        "minor": "4",
                        "type": "R"
                    },
                    "text": "14.1R4.8"
                }
            },
            "master": "RE0",
            "master_state": true,
            "model": "VMX",
            "model_info": {
                "re0": "VMX"
            },
            "personality": "MX",
            "re_info": {
                "default": {
                    "0": {
                        "last_reboot_reason": "0x10:misc hardware reason",
                        "mastership_state": "master",
                        "model": "RE-VMX",
                        "status": "OK"
                    },
                    "default": {
                        "last_reboot_reason": "0x10:misc hardware reason",
                        "mastership_state": "master",
                        "model": "RE-VMX",
                        "status": "OK"
                    }
                }
            },
            "re_master": {
                "default": "0"
            },
            "re_name": "re0",
            "serialnumber": "VM5543516AC4",
            "srx_cluster": null,
            "srx_cluster_id": null,
            "srx_cluster_redundancy_group": null,
            "switch_style": "BRIDGE_DOMAIN",
            "vc_capable": false,
            "vc_fabric": null,
            "vc_master": null,
            "vc_mode": null,
            "version": "14.1R4.8",
            "version_RE0": "14.1R4.8",
            "version_RE1": null,
            "version_info": {
                "build": 8,
                "major": [
                    14,
                    1
                ],
                "minor": "4",
                "type": "R"
            },
            "virtual": true
        }
    },
    "changed": false,
    "facts": {
        "HOME": "/var/home/neteng",
        "RE0": {
            "last_reboot_reason": "0x10:misc hardware reason",
            "mastership_state": "master",
            "model": "RE-VMX",
            "status": "OK",
            "up_time": "11 minutes, 45 seconds"
        },
        "RE1": null,
        "RE_hw_mi": false,
        "current_re": [
            "master",
            "node",
            "fwdd",
            "member",
            "pfem",
            "re0"
        ],
        "domain": "kpn",
        "fqdn": "ASH1-INT-03.kpn",
        "has_2RE": false,
        "hostname": "ASH1-INT-03",
        "hostname_info": {
            "re0": "ASH1-INT-03"
        },
        "ifd_style": "CLASSIC",
        "junos_info": {
            "re0": {
                "object": {
                    "build": 8,
                    "major": [
                        14,
                        1
                    ],
                    "minor": "4",
                    "type": "R"
                },
                "text": "14.1R4.8"
            }
        },
        "master": "RE0",
        "master_state": true,
        "model": "VMX",
        "model_info": {
            "re0": "VMX"
        },
        "personality": "MX",
        "re_info": {
            "default": {
                "0": {
                    "last_reboot_reason": "0x10:misc hardware reason",
                    "mastership_state": "master",
                    "model": "RE-VMX",
                    "status": "OK"
                },
                "default": {
                    "last_reboot_reason": "0x10:misc hardware reason",
                    "mastership_state": "master",
                    "model": "RE-VMX",
                    "status": "OK"
                }
            }
        },
        "re_master": {
            "default": "0"
        },
        "re_name": "re0",
        "serialnumber": "VM5543516AC4",
        "srx_cluster": null,
        "srx_cluster_id": null,
        "srx_cluster_redundancy_group": null,
        "switch_style": "BRIDGE_DOMAIN",
        "vc_capable": false,
        "vc_fabric": null,
        "vc_master": null,
        "vc_mode": null,
        "version": "14.1R4.8",
        "version_RE0": "14.1R4.8",
        "version_RE1": null,
        "version_info": {
            "build": 8,
            "major": [
                14,
                1
            ],
            "minor": "4",
            "type": "R"
        },
        "virtual": true
    },
    "invocation": {
        "module_args": {
            "attempts": null,
            "baud": null,
            "config_format": null,
            "console": null,
            "cs_passwd": null,
            "cs_user": null,
            "host": "192.168.2.50",
            "level": null,
            "logdir": null,
            "logfile": null,
            "mode": null,
            "passwd": "VALUE_SPECIFIED_IN_NO_LOG_PARAMETER",
            "port": 830,
            "savedir": null,
            "ssh_config": null,
            "ssh_private_key_file": null,
            "timeout": 10,
            "user": "neteng"
        }
    }
}

TASK [Print version] *******************************************************************************************************************
task path: /home/neteng/ansible-play-books/get_2.yaml:10
ok: [vmx01] => {
    "junos.version": "14.1R4.8"
}
META: ran handlers
META: ran handlers

PLAY RECAP *****************************************************************************************************************************
vmx01                      : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

(tools) [neteng@devel ansible-play-books]$
```
